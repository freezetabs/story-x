from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
import time
from .views import homepage,signup,main

class unitTest(TestCase):
      def test_app_url_exist(self):
            response=Client().get('/')
            self.assertEqual(response.status_code,200)
      def test_view_homepage_exist(self):
            found=resolve('/')
            self.assertEqual(found.func, homepage)
      def test_view_signup_exist(self):
            found=resolve('/signup')
            self.assertEqual(found.func, signup)
      def test_app_url_signup_exist(self):
            response=Client().get('/signup')
            self.assertEqual(response.status_code,200)


class FunctionalTest(LiveServerTestCase):
   def setUp(self):
       chrome_options = Options()
       chrome_options.add_argument('--dns-prefetch-disable')
       chrome_options.add_argument('--no-sandbox')     
       chrome_options.add_argument('--disable-dev-shm-usage')           
       chrome_options.add_argument('--headless')
       chrome_options.add_argument('disable-gpu')
       self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
       super(FunctionalTest, self).setUp()

   def tearDown(self):
       self.selenium.quit()
       super(FunctionalTest, self).tearDown()

   def test_input_todo(self):
       selenium = self.selenium	
       selenium.get(self.live_server_url + '/')
       time.sleep(5)
       # find the form element
       signbut = selenium.find_element_by_id('signbut')


       signbut.send_keys(Keys.RETURN)
       time.sleep(5)

       
       Username = selenium.find_element_by_name('Username')
       Password = selenium.find_element_by_name('Password')

       Username.send_keys("TestPerson")
       Password.send_keys("password")

       signup= selenium.find_element_by_id('sign-up')

       signup.send_keys(Keys.RETURN)

       time.sleep(5)
       selenium.get(self.live_server_url + '/')


       usernameL=selenium.find_element_by_name('Username')
       passwordL=selenium.find_element_by_name('Password')

       usernameL.send_keys("TestPerson")
       passwordL.send_keys("password")

       loginL=selenium.find_element_by_id('log-in')

       loginL.send_keys(Keys.RETURN)
       time.sleep(5)

       checkname= self.selenium.find_element_by_tag_name('body').text

       self.assertIn('TestPerson', checkname)

       LogoutL= selenium.find_element_by_id('log-out')

       LogoutL.send_keys(Keys.RETURN)



       




   
   
   





