from django.urls import path
from . import views

app_name='homepage'
urlpatterns = [
    path('', views.homepage,name='homepage'),
    path('signup', views.signup,name='signup'),
    path('main', views.main,name='main'),
    path('logout',views.logout,name='logout')


]


