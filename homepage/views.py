from django.shortcuts import render,redirect
from django.http import JsonResponse
import json
import requests
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as django_logout
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse


def homepage(request):

	if(request.method=='GET'):

		return render(request, 'landing.html')
	else:
		username=request.POST.get("Username")
		password=request.POST.get("Password")

		user=authenticate(username=username,password=password)
		if(user is not None):
			login(request,user)
			return redirect('homepage:main')

		try:
			user = User.objects.get(username=username)
			messages.error(request, "Wrong password.")
			return render(request, 'landing.html')
		except:
			messages.error(request,"Username does not exist.")
			return render(request, 'landing.html')

def signup(request):

	if(request.method == 'GET'):
			return render(request, 'signup.html')
		
	else:
		username= request.POST.get("Username")
		password=request.POST.get("Password")
		try:
			user = User.objects.get(username=username)
			messages.error(request,"Sorry, username is not available.")
			return render(request,'signup.html')
		except:
			user=User.objects.create_user(username=username,password=password)
			messages.success(request,"Sign up successful.")

			
			return redirect(reverse('homepage:homepage'))


@login_required(login_url='homepage:homepage')
def main(request):
	if(request.method=='GET'):
		return render(request,'main.html')

def logout(request):
	django_logout(request)
	return redirect('homepage:homepage')








	    





 